# MATUP
Projekt je zaměřený na stvoření webové a mobilní aplikace, která umožňuje žákům přípravu na maturitní zkoušku z matematiky zábavnou, hravou formou.

#### E-mail pro kontakt a jakékoli dotazy: [matup.group@gmail.com](mailto:matup.group@gmail.com)

#### Více info: [Wiki projektu](https://gitlab.fel.cvut.cz/gulineka/is/-/wikis/home)
